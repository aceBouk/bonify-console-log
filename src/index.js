import { computeOptions, defaultOptions } from './utils/pluginOption';
import { isObject, matchesFile, toArray } from './utils/tools';

export default function({ types: t }) {
  const visitor = {
    CallExpression(path) {
      if (
        t.isMemberExpression(path.node.callee) &&
        path.node.callee.object.name === 'console'
      ) {
        // options need to be an object
        if (this.opts && !isObject(this.opts)) {
          return console.error(
            '[babel-plugin-console-bonify]: options need to be an object.'
          );
        }

        if (this.opts.exclude) this.opts.exclude = toArray(this.opts.exclude);
        if (this.opts.include) this.opts.include = toArray(this.opts.include);
        if (this.opts.windowProperty)
          this.opts.windowProperty = toArray(this.opts.windowProperty);

        const options = computeOptions(defaultOptions, this.opts);

        const filename = this.filename || this.file.opts.filename || 'unknown';

        // not work on excluded files, and exclude is proiority other than include
        if (
          Array.isArray(options.exclude) &&
          options.exclude.length &&
          matchesFile(options.exclude, filename)
        ) {
          return;
        }

        // just work on included files
        if (
          Array.isArray(options.include) &&
          options.include.length &&
          !matchesFile(options.include, filename)
        ) {
          return;
        }

        let description = '';

        if (path.node.arguments[0].value) {
          description = `Bonify rocks ${path.node.arguments[0].value}`
          path.node.arguments[0] = t.stringLiteral(description);
        }
      }
    }
  };

  return {
    name: 'babel-plugin-console-bonify',
    visitor
  };
}
