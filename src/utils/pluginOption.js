export const defaultOptions = {
  expression: 'console.log',
  exclude: ['node_modules'],
  include: []
};

/* Combines default options and user options */
export function computeOptions(defaultOptions, userOptions = {}) {
  return Object.assign({}, defaultOptions, userOptions);
}
