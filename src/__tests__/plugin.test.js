import { create } from 'babel-test';

const { test } = create({
  plugins: [require.resolve('../index.js')],
});

test('surcharge console.log args with prefix', async ({ transform }) => {
  const { code } = await transform("console.log('hello')");

  expect(code).toBe('console.log(\"Bonify rocks hello\");');
});
