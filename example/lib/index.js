"use strict";

var foo = () => {
  function fo() {
    console.log("Bonify rocks apple");
    console.log("Bonify rocks orange");
  }
};

class Foo {
  bar() {
    console.log("Bonify rocks banana");
  }

}

var obj = {
  1: console.log("Bonify rocks 1")
};
console.log("Bonify rocks just a test");