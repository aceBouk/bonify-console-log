const foo = () => {
  function fo() {
    console.log('apple');
    console.log('orange');
  }
};

class Foo {
  bar() {
    console.log('banana');
  }
}

const obj = {
  1: console.log('1')
}

console.log('just a test')
