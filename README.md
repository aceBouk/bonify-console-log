# Babel Plugin Bonify Console Log

A babel plugin for enhanced console logging behavior.

This plugin does not make any changes to the actual `console` object. It
simply provides convenience methods available in your development source
files. The transformed output effects the aspects such as the arguments
that are passed to the `console.log` methods.

# Installation

```
npm install --save-dev babel-plugin-bonify-console-log
```


# Configuration
Add plugin into **.babdelrc** or **babel.config.json**

```js
{
  plugins: ['babel-plugin-bonify-console-log']
}
```

# Howe it works

it surcharge any `console.log` arguments with a prefix wich is `Bonify rocks`

As an example :
```
  console.log('just a test')
```

will be transpiled to:
```
  console.log("Bonify rocks just a test");
```
See `/example` for more examples.
