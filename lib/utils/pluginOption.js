"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.computeOptions = computeOptions;
exports.defaultOptions = void 0;
const defaultOptions = {
  expression: 'console.log',
  exclude: ['node_modules'],
  include: []
};
/* Combines default options and user options */

exports.defaultOptions = defaultOptions;

function computeOptions(defaultOptions, userOptions = {}) {
  return Object.assign({}, defaultOptions, userOptions);
}